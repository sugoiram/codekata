﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeKata.Models
{
    public class TripDuration
    {
        public string Name { get; set; }

        public double Duration { get; set; }

        public double Distance { get; set; }
    }
}
