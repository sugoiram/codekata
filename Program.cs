﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using CodeKata.Models;
using System.Linq;

namespace CodeKata
{
    class Program
    {
        private const string usageText = "Usage: CodeKata inputfile.txt";
        private static List<string> DriverNames = new List<string>();

        private static List<TripDuration> Trips = new List<TripDuration>();
        static int Main(string[] args)
        {

            if (args.Length < 1)
            {
                Console.WriteLine(usageText);
                return 1;
            }

            Console.WriteLine("Opening input file...");

            string inputfilename = args[0];

            inputfilename = "inputfile.txt";


            //Open the stream and read it back.
            using (FileStream fs = File.OpenRead(inputfilename))
            {

                string[] commands;

                byte[] b = new byte[1024];
                UTF8Encoding temp = new UTF8Encoding(true);
                while (fs.Read(b, 0, b.Length) > 0)
                {

                    string line = temp.GetString(b);
                    Console.WriteLine(line);


                    commands = line.Split("\r\n");

                    Console.WriteLine("Processing...");


                    foreach (string command in commands)
                    {

                        string sCmd = "";

                        try
                        {
                            sCmd = command.Substring(0, command.IndexOf(" "));
                        }
                        catch(Exception ex)
                        {
                            //Invalid row
                        }
                        

                        if (sCmd.Equals("Driver"))
                        {
                            SaveDriver(command);
                        }
                        else if (sCmd.Equals("Trip"))
                        {
                            Calculate(command);
                        }

                        //System.Console.Write("{0} ", command);
                    }

                }
            }


            var totalbyDriver =
                from trip in Trips
                group trip by trip.Name into tripGroup
                select new
                {
                    Name = tripGroup.Key,
                    TotalDistance = tripGroup.Sum(x => x.Distance),
                    TotalDuration = tripGroup.Sum(x => x.Duration),
                };


            var orderedList = totalbyDriver.OrderBy(x => x.TotalDistance).OrderByDescending(x => x.TotalDistance).ToList();

            Console.WriteLine("Output:");



            foreach (var element in orderedList)
            {


                if (element.TotalDuration == 0)
                {
                    Console.WriteLine(element.Name + ": 0 miles");
                }
                else
                {
                    Console.WriteLine(element.Name + ": " + Convert.ToInt32(element.TotalDistance) + " miles @ " + Convert.ToInt32(element.TotalDistance / element.TotalDuration) + " mph");
                }

            }


            return 0;
        }

        public static void SaveDriver(string driver)
        {
            string drivername = driver.Substring(driver.IndexOf(" "));
            DriverNames.Add(drivername.Trim());

            //Add default value
            TripDuration td = new TripDuration { Name = drivername.Trim(), Duration = 0, Distance = 0 };
            Trips.Add(td);
        }

        public static void Calculate(string trip)
        {
            string[] strip = trip.Split(" ");

            string[] sInitialTime = strip[2].Split(":");

            string[] sFinalTime = strip[3].Split(":");

            TimeSpan timeSpanInitial = new TimeSpan(Convert.ToInt32(sInitialTime[0]), Convert.ToInt32(sInitialTime[1]), 0);

            TimeSpan timeSpanFinal = new TimeSpan(Convert.ToInt32(sFinalTime[0]), Convert.ToInt32(sFinalTime[1]), 0);

            TimeSpan period = timeSpanFinal -timeSpanInitial;

            TripDuration td = new TripDuration { Name = strip[1], Duration = period.Hours + Convert.ToDouble(period.Minutes) / 60, Distance = Convert.ToDouble(strip[4]) };

            Trips.Add(td);
        }
    }


}
